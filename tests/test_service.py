
from mbuslite import Service



def test_subclasses_are_singletons():
    class Sub( Service ):
        pass

    assert Sub() is Sub()



def test_subclasses_are_independent():
    class Sub1( Service ):
        pass

    class Sub2( Service ):
        pass

    assert Sub1() is not Sub2()

