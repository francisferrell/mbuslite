
from pytest import fixture

from mbuslite.bus import MessageBus



@fixture
def bus():
    return MessageBus()



class Handler:

    def __init__( self ):
        self.was_called = False
        self.args = None
        self.kwargs = None


    def __call__( self, *args, **kwargs ):
        self.was_called = True
        self.args = args
        self.kwargs = kwargs



class UnhandledException( Exception ):
    pass

class RaisesExceptionHandler( Handler ):

    def __call__( self, *args, **kwargs ):
        super().__call__( *args, **kwargs )
        raise UnhandledException



def test_publish_requires_no_subscribers( bus ):
    """
    Publishing succeeds without error when there are no subscribers.
    """
    bus.publish( 'test' )



def test_publish_calls_subscriber( bus ):
    """
    Publishing calls the subscriber.
    """
    foo = Handler()
    bus.subscribe( 'foo', foo )
    bus.publish( 'foo' )
    assert foo.was_called



def test_publish_does_not_call_other_subscribers( bus ):
    """
    Publishing does not call the subscribers of other topics.
    """
    foo = Handler()
    bus.subscribe( 'foo', foo )
    bus.publish( 'bar' )
    assert not foo.was_called



def test_publish_passes_arguments( bus ):
    """
    Publishing passes along the arguments to the subscriber.
    """
    foo = Handler()
    bus.subscribe( 'foo', foo )
    bus.publish( 'foo', 42, key = 'foo' )
    assert foo.args[0] == 42
    assert foo.kwargs['key'] == 'foo'



def test_publish_calls_all_subscribers( bus ):
    """
    Publishing calls all of the subscribers of the given topic.
    """
    foo1 = Handler()
    foo2 = Handler()
    bus.subscribe( 'foo', foo1 )
    bus.subscribe( 'foo', foo2 )
    bus.publish( 'foo' )
    assert foo1.was_called
    assert foo2.was_called



def test_unsubscribe( bus ):
    """
    Unsubscribing prevents future publishing from calling the subscriber.
    """
    foo = Handler()
    subscription = bus.subscribe( 'foo', foo )
    subscription.unsubscribe()
    bus.publish( 'foo' )
    assert not foo.was_called



def test_publish_continues_past_an_exception( bus ):
    """
    Publishing calls all handlers, even if an earlier handler raises an exception.
    """
    foo1 = RaisesExceptionHandler()
    foo2 = Handler()
    bus.subscribe( 'foo', foo1 )
    bus.subscribe( 'foo', foo2 )
    bus.publish( 'foo' )
    assert foo1.was_called
    assert foo2.was_called

