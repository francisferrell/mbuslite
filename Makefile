
requirements.build.txt: requirements.build.in
	$(eval VENV := $(shell mktemp --directory --tmpdir=. make-requirements.build.txt.XXXXXX))
	python3 -m venv "$(VENV)"
	"$(VENV)/bin/pip3" install -r requirements.build.in
	echo '# This file is auto-generated, do not edit it' >requirements.build.txt
	echo '# edit requirements.build.in' >>requirements.build.txt
	echo '# then run make requirements.build.txt' >>requirements.build.txt
	"$(VENV)/bin/pip3" freeze --all >>requirements.build.txt
	rm -rf --one-file-system "$(VENV)"

venv/bin/activate: requirements.build.txt
	rm -rf venv
	python3 -m venv venv
	./venv/bin/pip install -r requirements.build.txt
	./venv/bin/pip install -e .

venv: venv/bin/activate

test: venv
	./venv/bin/python3 -m pytest

.PHONY: test

shell: venv
	./venv/bin/python3 -i

.PHONY: shell

dist: venv buildversion
	rm -rf dist/
	./venv/bin/python3 setup.py sdist bdist_wheel

.PHONY: dist


apidocs: venv
	rm -rf ./sphinx/_build/html
	sh -c '. venv/bin/activate && make -C sphinx html'

.PHONY: apidocs

serveapidocs: apidocs
	sh -c 'cd sphinx/_build/html/; python3 -m http.server'

.PHONY: serveapidocs


buildversion:
	@set -e ;\
		if [ -n "$${CI_COMMIT_TAG}" ] ; then \
			VERSION="$${CI_COMMIT_TAG#ubuntu/}" ;\
			echo "writing build version $${VERSION}" ;\
			sed -i -e "/^__version__\s*=/ s%=.*%= '$${VERSION}'%" $(CURDIR)/mbuslite/__init__.py ;\
		else \
			echo "CI_COMMIT_TAG not set in environment, skipping build version" ;\
		fi

.PHONY: buildversion



testpypi: venv
	@set -e ;\
		if [ -e ~/.pypirc ] ; then \
			echo "~/.pypirc already exists, skipping configuration" ;\
		elif ! env | grep -q '^TEST_PYPI_API_TOKEN=' ; then \
			echo "TEST_PYPI_API_TOKEN not set in environment, skipping ~/.pypirc configuration" ;\
		else \
			echo "configuring ~/.pypirc with TEST_PYPI_API_TOKEN" ;\
			echo '[testpypi]' >~/.pypirc ;\
			echo 'username = __token__' >>~/.pypirc ;\
			env | grep '^TEST_PYPI_API_TOKEN=' | sed 's/TEST_PYPI_API_TOKEN/password/' >>~/.pypirc ;\
		fi
	./venv/bin/twine upload --repository testpypi dist/*

.PHONY: testpypi



pypi: venv
	@set -e ;\
		if [ -e ~/.pypirc ] ; then \
			echo "~/.pypirc already exists, skipping configuration" ;\
		elif ! env | grep -q '^PYPI_API_TOKEN=' ; then \
			echo "PYPI_API_TOKEN not set in environment, skipping ~/.pypirc configuration" ;\
		else \
			echo "configuring ~/.pypirc with PYPI_API_TOKEN" ;\
			echo '[pypi]' >~/.pypirc ;\
			echo 'username = __token__' >>~/.pypirc ;\
			env | grep '^PYPI_API_TOKEN=' | sed 's/PYPI_API_TOKEN/password/' >>~/.pypirc ;\
		fi
	./venv/bin/twine upload dist/*

.PHONY: pypi




release:
	@if [ -n "$(VERSION)" ]; then true; else echo "Error: VERSION not specified. If you're calling 'make release' directly, make sure you know what you're doing!"; exit 2; fi
	@if [ "$$( git rev-parse --abbrev-ref HEAD )" = 'main' ]; then true; else echo "Error: branch is '$$( git rev-parse --abbrev-ref HEAD )'; expected 'main'"; exit 2; fi
	@echo Releasing $(VERSION)!
	@echo
	git fetch origin
	git tag '$(VERSION)'
	git checkout ubuntu
	git reset --hard origin/ubuntu
	git merge main --no-edit --message 'merge main into ubuntu for release of $(VERSION)'
	gbp dch --new-version='$(VERSION)-1' --release --distribution=groovy --force-distribution --commit --commit-msg='release $(VERSION) for ubuntu' --spawn-editor=never
	git tag 'ubuntu/$(VERSION)-1'
	git checkout main
	@echo
	@echo Release workflow complete. Now run:
	@echo
	@echo '       git push origin ubuntu'
	@echo '       git push --tags'
	@echo

.PHONY: release



revbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2,$$3+1}' )"

minorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1,$$2+1,0}' )"

majorbump:
	make release VERSION="$$( git tag | grep '^[0-9]' | sort --version-sort | tail -1 | awk 'BEGIN{FS=OFS="."}{print $$1+1,0,0}' )"

.PHONY: revbump minorbump majorbump



clean:
	rm -rf \
		build/ \
		dist/ \
		mbuslite.egg-info/ \
		.pytest_cache/ \
		sphinx/_build/ \
		venv/
	find . -type d -name __pycache__ -prune -exec rm -rf {} \;

.PHONY: clean

