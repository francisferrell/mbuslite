
import logging
import sys

try:
    level = sys.argv[1].upper()
except IndexError:
    level = 'WARNING'

logging.basicConfig()
logging.getLogger( 'mbuslite' ).setLevel( level )

from mbuslite import Bus

logger = logging.getLogger( 'example' )
logger.setLevel( 'INFO' )



class Producer:

    def announce( self ):
        Bus.publish( 'topic.name', 'Hello, consumers!' )



class Consumer:

    def __init__( self ):
        Bus.subscribe( 'topic.name', self.on_message )

    def on_message( self, msg ):
        logger.info( msg )



class BadConsumer( Consumer ):

    def on_message( self, msg ):
        dict()['foo']



consumer1 = Consumer()
consumer2 = BadConsumer()
consumer3 = Consumer()
producer = Producer()
producer.announce()

