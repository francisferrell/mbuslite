
from mbuslite import Bus



class Producer:

    def announce( self ):
        Bus.publish( 'topic.name', 'Hello, consumers!' )



class Consumer:

    def __init__( self ):
        Bus.subscribe( 'topic.name', self.on_message )

    def on_message( self, msg ):
        print( msg )



consumer1 = Consumer()
consumer2 = Consumer()
consumer3 = Consumer()
producer = Producer()
producer.announce()

